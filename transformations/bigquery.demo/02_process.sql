CREATE OR REPLACE TABLE `tr`.`out_order_lines` AS
SELECT
    d.`orderlinenumber`,
    o.`ordernumber`,
    o.`orderdate`,
    o.`shippeddate`,
    d.`productcode`,
    d.`quantityordered`,
    d.`total_price`,
    d.`priceeach`
FROM `tr`.`cln_orders` o
LEFT JOIN `tr`.`cln_orderdetails` d ON o.`ordernumber` = d.`ordernumber`
;

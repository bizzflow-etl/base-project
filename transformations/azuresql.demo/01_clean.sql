SELECT
    "ordernumber",
    "orderdate",
    "requireddate",
    CASE
        WHEN "shippeddate" = '' THEN '1970-01-01'
        ELSE "shippeddate"
    END as "shippeddate",
    "status"
INTO "tr"."cln_orders"
FROM
    "tr"."in_orders";

SELECT
    "ordernumber",
    "productcode",
    "quantityordered",
    "priceeach",
    "orderlinenumber",
    CAST("quantityordered" as INT) * CAST("priceeach" AS FLOAT) as "total_price"
INTO "tr"."cln_orderdetails"
FROM
    "tr"."in_orderdetails"
;

SELECT
    d."orderlinenumber",
    o."ordernumber",
    o."orderdate",
    o."shippeddate",
    d."productcode",
    d."quantityordered",
    d."total_price",
    d."priceeach"
INTO "tr"."out_order_lines"
FROM "tr"."cln_orders" o
LEFT JOIN "tr"."cln_orderdetails" d ON o."ordernumber" = d."ordernumber"
;

# Bizzflow Demo Project

<!-- \> [Guidepost](guidepost.md) | Bizzflow Demo Project | [Project Design](project-design.md) -->

[[_TOC_]]

---

This repository contains demo project for Bizzflow R2 and can be run within
[Google Cloud Platform](https://console.cloud.google.com) with BigQuery analytical warehouse. In order to setup your
Bizzflow installation, please refer to [the official install guide](https://wiki.bizzflow.net/#/install-guide).

## About this guide

This guide is divided into multiple sections and should walk you through all the basic steps that you need to take
in order to extract data from your data source, transform them in the analytical warehouse and create a separate
datamart in order to connect to the output data.

### Order of reading

You will find multiple links throughout the guide that will lead you in the order we thought made the most sense.
The guide is linear, so you should not be afraid to miss a thing. If you ever need to go back to review past steps
or are too curious and want to look a little bit ahead, you can use the guide's [Guidepost](toc.md)
or breadcrumbs navigation in the header of every chapter.

The designed pace and order is denoted using [Next](.) and [Previous](.) links at the bottom of
every page.

### Formattings used

Throughout the *guide*, **these are the formattings used**:

[This is a link](.)

> This is a hint, you should keep this in mind.

`This is a reserved word or a keyword`

```plain
This is a code
```

```python
print("This code is highlighted")
```

#### This is what file samples will look like

---

`main.py`

```python
from awesome import stuff

if __name__ == "__main__":
    stuff.happen()
```

## Ready, set, go!

And that's pretty much it. You are ready to go.

[Environment (Next)](environment.md)
